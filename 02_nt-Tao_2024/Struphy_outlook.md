# Near term future of Struphy (6-12 months)

## Performance

* **GPU support for Struphy kernerls** (Max Lindquist, 6 Months ACH support)

* **pyAMRex backend (efficient PIC data structures)** (Matilde Tozzi, Master thesis, 6-8 months)
  * automatic load balancing
  * GPU through Kokkos

* **exploring PETSc backend for linear solvers** (Max Lindquist, 6 Months ACH support)
  * Use psydac's `to_petsy()` on StencilMatrices, maybe need some interface coding for composition of matrices

## Algorithms

* **Local projectors (quasi-interpolation)** (Nathan Marin, PhD thesis in year 1)
  * sparse output (Stencil format) of projections
  * enables porting Hodge operators (`BasisProjectionOperators`) to PETSc

* **geometric multigrid for Poisson, Shear-Alfven step** (Nathan Marin, PhD thesis in year 1)
  * Use the Derham class to easily assemble the hierearchy of discretizations
  * implement the preojection/reconstruction operators

* **Saddle point solver (Uzawa algorithm)** (Philipp Thalhofer, Master thesis, 6-8 months)
  * general class `SaddlePointSolver` for saddle point problems
  * application to quasi-neutrality in two-fluid model (Omar, Marco)

* **Hamiltonian delta-f discretizations** (Dominik Bell, PhD year 2)
  * nonlinear Vlasov-Maxwell
  * linear gyrokinetic models 

* **Meshless methods (SPH) for Euler and MHD equations** (Valentin Carlier, PhD year 3 and Amin Raiessi)
  * particle sorting algorithms
  * meshless Euler, with noise (Amin)
  * meshless ideal MHD

## Physics

* **Paper: comparison of CC, PC and drift-kinetic CC for TAEs** (Byung Kyu Na, PhD year 2)

* **MHD equations in time-varying magnetic fieds from external coils** (nt-Tao collboration)
  * new class `CoilFields` interfaces to Biot-Savart solvers (RatGUI)
  * New propagators, models to account for $B_{ext}(t)$