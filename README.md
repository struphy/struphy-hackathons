# Struphy Hackathons <img src="dog-cartoon-struphy.jpg" width="150" align="right"> 

Join or review some of the latest public [Struphy](https://gitlab.mpcdf.mpg.de/struphy/struphy) events:

* [1st Struphy Hackathon](https://gitlab.mpcdf.mpg.de/struphy/struphy-hackathons/-/blob/main/01_NumKin2023/welcome.md), NumKin 2023: November 10th 2023 in Garching.

struphy/struphy/-/tree/devel/doc
