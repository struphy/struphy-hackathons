## 1. Install Struphy

Choose one of the three options mentioned in the doc: https://struphy.pages.mpcdf.de/struphy/sections/install.html#installation.

Make sure to use a [virtual Python environment](https://struphy.pages.mpcdf.de/struphy/sections/install.html#virtual-python-environment) in order to not interfere with the personal Python setup on your machine.

In case you cannot fulfill the [requirements](https://struphy.pages.mpcdf.de/struphy/sections/install.html#requirements) on your machine, you can opt for the [installation using Docker](https://struphy.pages.mpcdf.de/struphy/sections/install.html#docker). 

Type `struphy -h` to see if the installation was successful.

Do not forget to `struphy compile` before first using Struphy (this will trigger the necessary Psydac installation on first call).

## 2. Complete the Quickstart guide

Follow the [Qickstart guide](https://struphy.pages.mpcdf.de/struphy/sections/quickstart.html#quickstart) in the doc.

In case of questions/problems, one of the developers will be quick to assist, either in the room or via the [Struphy-developers channel](https://chat.gwdg.de/channel/struphy-developers) on RocketChat.

## 3. Solving some PDE models

First, clone the `struphy-simulations` repo:
```
git clone https://gitlab.mpcdf.mpg.de/struphy/struphy-simulations.git
```
The folders of this repo contain parameter files for Struphy runs. In particular, let's go to
```
cd struphy-simulations/1st_Hackathon_2023/
```
There should be four parameter files:
```
driftkinetic.yml  linmhd.yml  maxwell.yml  vlasov.yml
```
Make this the default I/O path, and go back:
```
struphy --set-i .
struphy --set-o .
cd ../..
```

### 3a. Maxwell

Let us run with the just downloaded parameter file on two MPI processes:
```
struphy run Maxwell -i maxwell.yml -o sim_1 --mpi 2
```
Next, we post-process the simulation data:
```
struphy pproc -d sim_1
```
For diagnostics, create a new jupyter notebook (`.ibynb`) on the same level as your virtual python environment folder. In this way you can choose the virtualenv as the kernel in your new notebook. In the notebook, we only need Python cells.

In the first cell, let us find out the Struphy I/O paths:
```
!struphy -p
```
Next, we set the correct path to the simulation output folder and load the binaries containing the field an grid data:
```
import os
import pickle

path_out = os.path.join('/your_working_dir/struphy-simulations/1st_Hackathon_2023/sim_1')

# load data dicts for e_field
with open(os.path.join(path_out, 'post_processing', 'fields_data', 'em_fields', 'e1_log.bin'), 'rb') as handle:
    point_data_log = pickle.load(handle)

# load grid
with open(os.path.join(path_out, 'post_processing', 'fields_data', 'grids_log.bin'), 'rb') as handle:
    grids_log = pickle.load(handle)

with open(os.path.join(path_out, 'post_processing', 'fields_data', 'grids_phy.bin'), 'rb') as handle:
    grids_phy = pickle.load(handle)
```
We can now verify the Maxwell dispersion relation using the Struphy diagnostics routine `power_spectrum_2d`:
```
from struphy.diagnostics.diagn_tools import power_spectrum_2d

# fft in (t, z) of first component of e_field on physical grid
power_spectrum_2d(point_data_log,
                  'e1',
                  'Maxwell',
                  grids_log,
                  grids_mapped=grids_phy,
                  component=0,
                  slice_at=[0, 0, None],
                  do_plot=True,
                  disp_name='Maxwell1D')
```

### 3b. LinearMHD

Run on two MPI processes and save to `sim_2`:
```
struphy run LinearMHD -i linmhd.yml -o sim_2 --mpi 2
```
then post-process the simulation data:
```
struphy pproc -d sim_2
```
In our jupyter notebook (`.ibynb`), we first need to set the correct path to the simulation output folder. 

Then we proceed similarly to Maxwell, except that we need some MHD equilibirum data from the parameter file to display the correct dicpersion relation:
```
path_out = '/your_working_dir/struphy-simulations/1st_Hackathon_2023/sim_2'
params_path = '/your_working_dir/struphy-simulations/1st_Hackathon_2023/sim_2/parameters.yml'

import yaml

with open(params_path) as file:
    parameters = yaml.load(file, Loader=yaml.FullLoader)
    
parameters
```
and
```
B0x = parameters['mhd_equilibrium']['HomogenSlab']['B0x']
B0y = parameters['mhd_equilibrium']['HomogenSlab']['B0y']
B0z = parameters['mhd_equilibrium']['HomogenSlab']['B0z']

p0 = parameters['mhd_equilibrium']['HomogenSlab']['beta'] * (B0x**2 + B0y**2 + B0z**2) / 2
n0 = parameters['mhd_equilibrium']['HomogenSlab']['n0']

disp_params = {'B0x': B0x,
               'B0y': B0y,
               'B0z': B0z,
               'p0': p0,
               'n0': n0,
               'gamma': 5/3}
```
Now let's load the binaries containing the field an grid data:
```
# load data dicts for velocity and pressure
with open(os.path.join(path_out, 'post_processing', 'fields_data', 'mhd', 'u2_log.bin'), 'rb') as handle:
    u2_log = pickle.load(handle)
    
with open(os.path.join(path_out, 'post_processing', 'fields_data', 'mhd', 'p3_log.bin'), 'rb') as handle:
    p3_log = pickle.load(handle)

# load grid
with open(os.path.join(path_out, 'post_processing', 'fields_data', 'grids_log.bin'), 'rb') as handle:
    grids_log = pickle.load(handle)

with open(os.path.join(path_out, 'post_processing', 'fields_data', 'grids_phy.bin'), 'rb') as handle:
    grids_phy = pickle.load(handle)
```
and verify the MHD dispersion relation using the Struphy diagnostics routine `power_spectrum_2d`. First, the shear Alfven branch:
```
power_spectrum_2d(u2_log,
                  'u2',
                  'LinearMHD',
                  grids_log,
                  grids_mapped=grids_phy,
                  component=0,
                  slice_at=[0, 0, None],
                  do_plot=True,
                  disp_name='Mhd1D',
                  disp_params=disp_params)
```
and then the sonic branches via the pressure:
```
power_spectrum_2d(p3_log,
                  'p3',
                  'LinearMHD',
                  grids_log,
                  grids_mapped=grids_phy,
                  component=0,
                  slice_at=[0, 0, None],
                  do_plot=True,
                  disp_name='Mhd1D',
                  disp_params=disp_params)
```

### 3c. Vlasov (PIC)

Run, save to `sim_3`:
```
struphy run Vlasov -i vlasov.yml -o sim_3
```
and post-process:
```
struphy pproc -d sim_3
```
In our jupyter notebook (`.ibynb`), we first need to set the correct path to the simulation output folder and load the simulation parameters:
```
path_out = '/your_working_dir/struphy-simulations/1st_Hackathon_2023/sim_3'
params_path = '/your_working_dir/struphy-simulations/1st_Hackathon_2023/sim_3/parameters.yml'

import yaml

with open(params_path) as file:
    parameters = yaml.load(file, Loader=yaml.FullLoader)
    
parameters
```
We then check the number of time steps:
```
import numpy as np

tgrid = np.load(os.path.join(path_out, 'post_processing', 't_grid.npy'))
Nt = len(tgrid) - 1
log_Nt = int(np.log10(Nt)) + 1

Nt
```
and transform the Cartesian (x,y,z)-corrdinates to (R,y,z)-coordinates:
```
Np = parameters['kinetic']['ions']['save_data']['n_markers']
print(Np)
    
pos = np.zeros((Nt + 1, Np, 3), dtype=float)

for n in range(Nt + 1):

    # load x, y, z coordinates
    orbits_path = os.path.join(path_out, 'post_processing', 'kinetic_data', 'ions', 'orbits', 'ions_{0:0{1}d}.txt'.format(n, log_Nt))
    pos[n] = np.loadtxt(orbits_path, delimiter=',')[:, 1:]

    # convert to R, y, z, coordinates
    pos[n, :, 0] = np.sqrt(pos[n, :, 0]**2 + pos[n, :, 1]**2)
```
Then, we want to recreate the mapped domain and the MHD equilibirum of the run:
```
from struphy.io.setup import setup_domain_mhd

domain, mhd_equil = setup_domain_mhd(parameters)
```
We first plot the absolute value of the background field:
```
from matplotlib import pyplot as plt

fig = plt.figure(figsize=(10, 8))
ax = fig.add_subplot(1, 1, 1)

e1 = np.linspace(0., 1., 101)
e2 = np.linspace(0., 1., 101)
e1[0] += 1e-5
X = domain(e1, e2, 0.)

im1 = ax.contourf(X[0], X[2], mhd_equil.absB0(e1, e2, 0.), levels=51)
ax.axis('equal')
ax.set_title('abs(B) at $\phi=0$')
fig.colorbar(im1)
ax.set_xlabel('R')
ax.set_ylabel('Z')
```
and then add the marker (R,Z)-coordinates:
```
for i in range(pos.shape[1]):
    ax.scatter(pos[:, i, 0], pos[:, i, 2], s=1)
   
ax.set_title('Passing and trapped particles (co- and counter direction)')
    
fig
```
