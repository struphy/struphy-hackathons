# Welcome to the 1st Struphy Hackathon!

Solving plasma physics PDEs with Python - from prototyping to production.

[Struphy](https://struphy.pages.mpcdf.de/struphy/index.html) - Structure-preserving hybrid codes - is an open-source Python package designed for solving PDEs with particle-in-cell (PIC) and geometric finite element methods (finite-element exterior calculus or FEEC).

**The development of Struphy is open to everybody**. At the moment it is lead by the division "Numerical Methods for Plasma Physics" at IPP Garching.

The Struphy mission:

* provide a fast and easy-to-use research tool, for free
* compound the programming of researchers
* teach important software development skills (git, write doc and test, work in a dev team)
* enthuse students for plasma physics simulations
* spread the open-source paradigm in the academic community

In this first Struphy workshop we will introduce the package and the philosophy behind it. Moreover, we shall engage in some hands-on experience by solving toy models like Poisson, Maxwell, Vlasov and linear MHD equations in various setups. We then give an outline of how to add a new model PDE to the package in a few simple steps.  

The workshop takes place on **Friday Nov. 10th from 9am-3pm** in the **Arnulf Schlüter lecture hall (D2 ground floor)** of the Max Planck Institute of Plasma Physics (IPP) in Garching, Germany. 

It is possible to follow the workshop online via the following link:

[Struphy zoom link](https://eu02web.zoom-x.de/j/61181454390?pwd=TnRQSzVrMUtvUVVnaXBpSWkvM2c4dz09)

The link will be active only during the duration of the workshop, starting at 8:30 on Nov. 10th.


## Program

*  9:00 -  9:30: Introduction by Struphy development team
*  9:30 - 10:30: `pip install struphy` and Quickstart
* 10:30 - 11:00: Struphy hands-on: solving some toy problems (Poisson, Maxwell, Vlasov, Linear MHD)
* 11:00 - 12:00: Adding new PDE models
* 12:00 - 13:00: Lunch break
* 13:00 - 14:00: Jupyter notebook tutorials I (data structures and plotting)
* 14:00 - 15:00: Jupyter notebook tutorials II (Tokamak geometry, power sprectra, MHD equilibria)

Check out some [hands-on tasks here](https://gitlab.mpcdf.mpg.de/struphy/struphy-hackathons/-/blob/main/01_NumKin2023/tasks.md)!

## Contact

* [Stefan Possanner](stefan.possanner@ipp.mpg.de)
* [Eric Sonnendrücker](sonnen@ipp.mpg.de)

Join the [Struphy-developers channel](https://chat.gwdg.de/channel/struphy-developers) on RocketChat during the workshop for questions and material.


## Material

Download the [Jupyter notebook tutorials](https://gitlab.mpcdf.mpg.de/struphy/struphy/-/tree/devel/doc/tutorials) from the Struphy repo.


## Struphy abstract

### Overview

**Struphy provides easy access to partial differential equations (PDEs) in plasma physics.
The package combines** *performance* **(for HPC),** *flexibility* **(models and physics features)
and** *usabilty* **(documentation).**  

*Performance* in Struphy is achieved using three building blocks:

   * [numpy](https://numpy.org/) (vectorization)
   * [mpi4py](https://pypi.org/project/mpi4py/) (parallelization)
   * [pyccel](https://github.com/pyccel/pyccel) (compilation)

Heavy computational kernels are pre-compiled using the Python accelerator [pyccel](https://github.com/pyccel/pyccel),
which on average shows [better performance](https://github.com/pyccel/pyccel-benchmarks) than *Pythran* or *Numba*.

*Flexibility* comes through the possibility of applying different [models](https://struphy.pages.mpcdf.de/struphy/sections/models.html) to a plasma physics problem.
Each model can be run on different [mapped domains](https://struphy.pages.mpcdf.de/struphy/sections/domains.html) and can load a variety of [MHD equilibria](https://struphy.pages.mpcdf.de/struphy/sections/mhd_equils.html),
[kinetic backgrounds](https://struphy.pages.mpcdf.de/struphy/sections/kinetic_backgrounds.html) and [initial conditions](https://struphy.pages.mpcdf.de/struphy/sections/inits.html).

*Usability* is guaranteed by Struphy's intuitive console interface (see for example :ref:`quickstart`).
Moreover, an extensive, maintained documentation is provided. In addition, you can learn Struphy
through a series of Jupyter notebook tutorials (see this workshop).

Struphy is an object-oriented code. The concept of [inheritance](https://www.w3schools.com/python/python_inheritance.asp) 
is heavily used in its basic design; it enables the streamlined addition of new models/features to Struphy.
For details, please check

   * [Adding a new Struphy model](https://struphy.pages.mpcdf.de/struphy/sections/developers.html#add-model)

Model discretization is based on finite element exterior calculus (FEEC) for the fluid/field quantities
and particle-in-cell (PIC) methods for the kinetic species. An overview of these methods is given here.
For the FEEC data structures, Struphy uses the open source library 

   * [Psydac](https://github.com/pyccel/psydac) (FEEC spaces)

Check out the full [Struphy documentation](https://struphy.pages.mpcdf.de/struphy/index.html) for further details.


### Origins

Struphy was developed out of [Florian Holderied's PhD thesis](https://mediatum.ub.tum.de/doc/1656539/document.pdf) focused on the simulation of energetic particles in fusion plasmas by means of MHD-kinetic hybrid models. The package has since been constantly developed and maintained by NMPP members, in collaboration with the MHD group of TOK.

Struphy went open source and [on PyPI](https://pypi.org/project/struphy/) in April 2023.

The original aim was to conserve Florian's work such that other researchers can build upon it. Indeed, new Master and PhD students quickly joined the project and developed it further, in particular Byung Kyu Na, Dominik Bell, Joris Thiel, Nathan Marín and Yingzhe Li.
